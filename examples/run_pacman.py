import os
import sys

from pacman import pacmanMain

if __name__ == '__main__':
    """
    The main function called when pacmanMain.py is run
    from the command line:

    > python pacmanMain.py

    See the usage string for more details.

    > python pacmanMain.py --help
    """
    args = pacmanMain.readCommand(sys.argv[1:])  # Get game components based on input
    pacmanMain.runGames(**args)

    # import cProfile
    # cProfile.run("runGames( **args )")
    pass
