#! /usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = 'vimostan@gmail.com'


from setuptools import setup

setup(
    name="pacman",
    version="0.0.1",
    description="Reinforcement learning with Pacman game.",
    license="BSD",
    keywords="Pacman",
    url="",
    packages=['pacman'],
    include_package_data=True,
)